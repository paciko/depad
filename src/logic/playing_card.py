from enum import Enum
from typing import Optional


class PlayingCardColor(Enum):
    WILD = 0
    RED = 1
    YELLOW = 2
    GREEN = 3
    BLUE = 4

class PlayingCardType(Enum):
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    SIX = 6
    SIDE = 7
    DRAWONE = 8
    GIVETWO = 9
    WILD = 10
    DRAWTHREE = 11
    SWAP = 12
    JOKER = 13
    SELFDEFENSE = 14

class PlayingCard:
    color: PlayingCardColor
    type: PlayingCardType

    def __init__(self, color: PlayingCardColor, pctype: PlayingCardType):
        if color == PlayingCardColor.WILD and pctype not in [PlayingCardType.WILD, PlayingCardType.DRAWTHREE, PlayingCardType.SWAP, PlayingCardType.JOKER, PlayingCardType.SELFDEFENSE]:
            raise Exception('cannot create WILD colored card of type ' + str(pctype))
        if color != PlayingCardColor.WILD and pctype not in [PlayingCardType.ONE, PlayingCardType.TWO, PlayingCardType.THREE, PlayingCardType.FOUR, PlayingCardType.FIVE, PlayingCardType.SIX, PlayingCardType.SIDE, PlayingCardType.DRAWONE, PlayingCardType.GIVETWO]:
            raise Exception('cards of type ' + str(pctype) + ' must be WILD colored')
        self.color = color
        self.type = pctype

    def can_be_put_on_top_of(self, other: 'PlayingCard', chosen_color: Optional[PlayingCardColor]):
        return self.color == other.color or self.type == other.type or self.color == PlayingCardColor.WILD or other.color == PlayingCardColor.WILD and self.color == chosen_color

    def can_be_defended(self):
        return self.type in PlayingCardType.DRAWONE, PlayingCardType.GIVETWO, PlayingCardType.DRAWTHREE, PlayingCardType.SWAP, PlayingCardType.JOKER, PlayingCardType.SELFDEFENSE

