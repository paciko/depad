from typing import Optional

from src.logic.player import Player
from src.logic.playing_card import PlayingCard, PlayingCardColor, PlayingCardType
import random


class DepadGame:
    players: [Player]
    pile: [PlayingCard]
    discard_pile: [PlayingCard]
    backward: bool
    active_player_index: int
    finished: bool
    winners: Optional[list[int]]
    chosen_color: Optional[PlayingCardColor]

    def __init__(self, num_players):
        self._initialize_pile()
        self._initialize_players(num_players)
        self._initially_distribute_cards()
        self.backward = False
        self.finished = False
        self.winners = None
        self.chosen_color = None

    def _initialize_pile(self):
        self.pile = []
        self.discard_pile = []
        for color in [PlayingCardColor.RED, PlayingCardColor.GREEN, PlayingCardColor.BLUE, PlayingCardColor.YELLOW]:
            for pctype in [PlayingCardType.ONE, PlayingCardType.TWO, PlayingCardType.THREE, PlayingCardType.FOUR,
                           PlayingCardType.FIVE, PlayingCardType.SIX]:
                for i in range(2):
                    self.pile.append(PlayingCard(color, pctype))
            for i in range(2):
                self.pile.append(PlayingCard(color, PlayingCardType.SIDE))
            self.pile.append(PlayingCard(color, PlayingCardType.DRAWONE))
            self.pile.append(PlayingCard(color, PlayingCardType.GIVETWO))

        for i in range(4):
            self.pile.append(PlayingCard(PlayingCardColor.WILD, PlayingCardType.WILD))
        for i in range(3):
            self.pile.append(PlayingCard(PlayingCardColor.WILD, PlayingCardType.DRAWTHREE))
        for i in range(2):
            self.pile.append(PlayingCard(PlayingCardColor.WILD, PlayingCardType.SELFDEFENSE))
            self.pile.append(PlayingCard(PlayingCardColor.WILD, PlayingCardType.SWAP))

        self.pile.append(PlayingCard(PlayingCardColor.WILD, PlayingCardType.JOKER))

        random.shuffle(self.pile)

        assert len(self.pile) == 76

    def _initialize_players(self, num_players: int):
        self.players = []
        for i in range(num_players):
            self.players.append(Player(''))  # TODO set name for players
        self.active_player_index = random.randrange(0, num_players)

    def _initially_distribute_cards(self):
        for player in self.players:
            for i in range(8):
                self._give_top_card_to_player(player)

    def _get_topmost_card(self):
        return self.discard_pile[-1]

    def start_game(self):
        self.discard_pile.append(self.pile.pop())
        while not self.finished:
            self._turn()

    def _turn(self):
        player = self.players[self.active_player_index]
        player.ask_turn()

    def _give_top_card_to_player(self, player: Player):
        card = self.pile.pop()
        player.give(card)
        if len(self.pile) == 0:
            random.shuffle(self.discard_pile)
            self.pile = self.discard_pile
            self.discard_pile = []

    def play_card(self, card: PlayingCard):
        if card.can_be_put_on_top_of(self._get_topmost_card(), self.chosen_color):
            self.discard_pile.append(card)
            self._check_if_game_finished()
            if self.finished:
                return True
            self._do_card_type_specific_stuff()
            if self.finished:
                return True
            return True
        else:
            return False

    def _check_if_game_finished(self):
        for i, player in enumerate(self.players):
            if len(player.cards_on_hand) == 0:
                self.finished = True
                self.winners = [i]

    def _get_index_of_next_player(self, index: int, players: [Player]):
        if self.backward:
            if index == 0:
                return len(players) - 1
            else:
                return index - 1
        else:
            if index == len(players) - 1:
                return 0
            else:
                return index + 1

    def _double_self_defense(self, aggressor, victim):  # returns tuple (aggressor, victim)
        self_defense = victim.self_defense()
        self._check_if_game_finished()
        if self.finished:
            return
        if self_defense:
            double_self_defense = aggressor.self_defense()
            self._check_if_game_finished()
            if self.finished:
                return
            if not double_self_defense:
                return victim, aggressor
        return (aggressor, victim)

    def _do_card_type_specific_stuff(self):
        # precondition: played card has already been appended to discard pile
        card: PlayingCard = self._get_topmost_card()
        player: Player = self.players[self.active_player_index]
        if card.type in [PlayingCardType.ONE, PlayingCardType.TWO, PlayingCardType.THREE, PlayingCardType.FOUR,
                         PlayingCardType.FIVE, PlayingCardType.SIX]:
            return
        elif card.type == PlayingCardType.SIDE:
            if len(self.players) > 2:  # there will be no difference between sides if only two players are playing
                self.backward = player.ask_which_side()
        elif card.type == PlayingCardType.DRAWONE:
            # ANY player can defend themself against this :(
            immune_player_index = self.active_player_index  # this holds the 1 player who does not have to draw a card
            i = immune_player_index
            remaining_players = self.players[:]
            while True:
                i = self._get_index_of_next_player(i, remaining_players)
                if i == immune_player_index:
                    break
                self_defense = self.players[i].self_defense()
                self._check_if_game_finished()
                if self.finished:
                    return
                if self_defense:
                    immune_player_index = i
                else:
                    self._give_top_card_to_player(self.players[i])
        elif card.type == PlayingCardType.GIVETWO:
            aggressor = player
            victim = player.choose_player()
            aggressor, victim = self._double_self_defense(aggressor, victim)
            if self.finished:
                return
            first_card = aggressor.choose_card_from_hand_and_remove()
            self._check_if_game_finished()
            if self.finished:
                return
            second_card = aggressor.choose_card_from_hand_and_remove()
            self._check_if_game_finished()
            if self.finished:
                return
            victim.give(first_card)
            victim.give(second_card)
        elif card.type in [PlayingCardType.WILD, PlayingCardType.SELFDEFENSE]:
            self.chosen_color = player.choose_color()
        elif card.type == PlayingCardType.DRAWTHREE:
            aggressor = player
            victim = player.choose_player()
            aggressor, victim = self._double_self_defense(aggressor, victim)
            if self.finished:
                return
            for i in range(3):
                self._give_top_card_to_player(victim)
        elif card.type == PlayingCardType.SWAP:
            will_swap: bool = player.ask_whether_effect()
            if will_swap:
                aggressor = player
                victim = player.choose_player()
                aggressor, victim = self._double_self_defense(aggressor, victim)
                if self.finished:
                    return
                if aggressor == player:  # if was not successfully defended against
                    tmp = victim.cards_on_hand
                    victim.cards_on_hand = aggressor.cards_on_hand
                    aggressor.cards_on_hand = tmp
            self.chosen_color = player.choose_color()
        elif card.type == PlayingCardType.JOKER:
            will_end: bool = False
            if len(player.cards_on_hand) <= 3:
                will_end = player.ask_whether_effect()
                if will_end:
                    self._finish_game_with_joker()
            self.chosen_color = player.choose_color()

    def _finish_game_with_joker(self):
        card_values = {
            PlayingCardType.ONE: 1,
            PlayingCardType.TWO: 2,
            PlayingCardType.THREE: 3,
            PlayingCardType.FOUR: 4,
            PlayingCardType.FIVE: 5,
            PlayingCardType.SIX: 6,
            PlayingCardType.SIDE: 5,
            PlayingCardType.DRAWONE: 5,
            PlayingCardType.GIVETWO: 5,
            PlayingCardType.WILD: 5,
            PlayingCardType.DRAWTHREE: 5,
            PlayingCardType.SELFDEFENSE: 5,
            PlayingCardType.SWAP: 5,
            PlayingCardType.JOKER: 5,
        }
        player_points = {}
        for player, i in self.players:
            for card in player.cards_on_hand:
                player_points[i] += card_values[card.type]

        self.finished = True
        winning_score: int = None
        for player_i, points in player_points.items():
            if winning_score is None or points < winning_score:
                winning_score = points

        self.winners = filter(lambda score: score == winning_score, player_points.keys())
