from src.logic.playing_card import PlayingCard

class Player:
    name: str
    cards_on_hand: [PlayingCard]

    def __init__(self, name):
        self.name = name
        self.cards_on_hand = []

    def give(self, card: PlayingCard):
        self.cards_on_hand.append(card)

    def ask_turn(self):
        pass

    def choose_player(self):
        pass

    def ask_whether_effect(self):
        pass

    def ask_which_side(self):
        pass

    def self_defense(self):
        pass

    def choose_color(self):
        pass