import unittest

from src.logic.game import DepadGame
from src.logic.player import Player
from src.logic.playing_card import PlayingCard, PlayingCardColor, PlayingCardType

class GameLogicTest(unittest.TestCase):
    def test_initialization(self):
        game = DepadGame(3)
        all_cards: [PlayingCard] = game.pile
        for player in game.players:
            all_cards.extend(player.cards_on_hand)
            self.assertEqual(8, len(player.cards_on_hand))

        with self.subTest():
            red1s = [card for card in all_cards if
                     card.color == PlayingCardColor.RED and card.type == PlayingCardType.ONE]
            self.assertEqual(2, len(red1s))
        with self.subTest():
            red2s = [card for card in all_cards if
                     card.color == PlayingCardColor.RED and card.type == PlayingCardType.TWO]
            self.assertEqual(2, len(red2s))
        with self.subTest():
            red3s = [card for card in all_cards if
                     card.color == PlayingCardColor.RED and card.type == PlayingCardType.THREE]
            self.assertEqual(2, len(red3s))
        with self.subTest():
            red4s = [card for card in all_cards if
                     card.color == PlayingCardColor.RED and card.type == PlayingCardType.FOUR]
            self.assertEqual(2, len(red4s))
        with self.subTest():
            red5s = [card for card in all_cards if
                     card.color == PlayingCardColor.RED and card.type == PlayingCardType.FIVE]
            self.assertEqual(2, len(red5s))
        with self.subTest():
            red6s = [card for card in all_cards if
                     card.color == PlayingCardColor.RED and card.type == PlayingCardType.SIX]
            self.assertEqual(2, len(red6s))
        with self.subTest():
            red_sides = [card for card in all_cards if
                         card.color == PlayingCardColor.RED and card.type == PlayingCardType.SIDE]
            self.assertEqual(2, len(red_sides))
        with self.subTest():
            red_d1s = [card for card in all_cards if
                       card.color == PlayingCardColor.RED and card.type == PlayingCardType.DRAWONE]
            self.assertEqual(1, len(red_d1s))
        with self.subTest():
            red_g2s = [card for card in all_cards if
                       card.color == PlayingCardColor.RED and card.type == PlayingCardType.GIVETWO]
            self.assertEqual(1, len(red_g2s))

        with self.subTest():
            green1s = [card for card in all_cards if
                       card.color == PlayingCardColor.GREEN and card.type == PlayingCardType.ONE]
            self.assertEqual(2, len(green1s))
        with self.subTest():
            green2s = [card for card in all_cards if
                       card.color == PlayingCardColor.GREEN and card.type == PlayingCardType.TWO]
            self.assertEqual(2, len(green2s))
        with self.subTest():
            green3s = [card for card in all_cards if
                       card.color == PlayingCardColor.GREEN and card.type == PlayingCardType.THREE]
            self.assertEqual(2, len(green3s))
        with self.subTest():
            green4s = [card for card in all_cards if
                       card.color == PlayingCardColor.GREEN and card.type == PlayingCardType.FOUR]
            self.assertEqual(2, len(green4s))
        with self.subTest():
            green5s = [card for card in all_cards if
                       card.color == PlayingCardColor.GREEN and card.type == PlayingCardType.FIVE]
            self.assertEqual(2, len(green5s))
        with self.subTest():
            green6s = [card for card in all_cards if
                       card.color == PlayingCardColor.GREEN and card.type == PlayingCardType.SIX]
            self.assertEqual(2, len(green6s))
        with self.subTest():
            green_sides = [card for card in all_cards if
                           card.color == PlayingCardColor.GREEN and card.type == PlayingCardType.SIDE]
            self.assertEqual(2, len(green_sides))
        with self.subTest():
            green_d1s = [card for card in all_cards if
                         card.color == PlayingCardColor.GREEN and card.type == PlayingCardType.DRAWONE]
            self.assertEqual(1, len(green_d1s))
        with self.subTest():
            green_g2s = [card for card in all_cards if
                         card.color == PlayingCardColor.GREEN and card.type == PlayingCardType.GIVETWO]
            self.assertEqual(1, len(green_g2s))

        with self.subTest():
            blue1s = [card for card in all_cards if
                      card.color == PlayingCardColor.BLUE and card.type == PlayingCardType.ONE]
            self.assertEqual(len(blue1s), 2)
        with self.subTest():
            blue2s = [card for card in all_cards if
                      card.color == PlayingCardColor.BLUE and card.type == PlayingCardType.TWO]
            self.assertEqual(len(blue2s), 2)
        with self.subTest():
            blue3s = [card for card in all_cards if
                      card.color == PlayingCardColor.BLUE and card.type == PlayingCardType.THREE]
            self.assertEqual(len(blue3s), 2)
        with self.subTest():
            blue4s = [card for card in all_cards if
                      card.color == PlayingCardColor.BLUE and card.type == PlayingCardType.FOUR]
            self.assertEqual(len(blue4s), 2)
        with self.subTest():
            blue5s = [card for card in all_cards if
                      card.color == PlayingCardColor.BLUE and card.type == PlayingCardType.FIVE]
            self.assertEqual(len(blue5s), 2)
        with self.subTest():
            blue6s = [card for card in all_cards if
                      card.color == PlayingCardColor.BLUE and card.type == PlayingCardType.SIX]
            self.assertEqual(len(blue6s), 2)
        with self.subTest():
            blue_sides = [card for card in all_cards if
                          card.color == PlayingCardColor.BLUE and card.type == PlayingCardType.SIDE]
            self.assertEqual(len(blue_sides), 2)
        with self.subTest():
            blue_d1s = [card for card in all_cards if
                        card.color == PlayingCardColor.BLUE and card.type == PlayingCardType.DRAWONE]
            self.assertEqual(len(blue_d1s), 1)
        with self.subTest():
            blue_g2s = [card for card in all_cards if
                        card.color == PlayingCardColor.BLUE and card.type == PlayingCardType.GIVETWO]
            self.assertEqual(len(blue_g2s), 1)

        with self.subTest():
            yellow1s = [card for card in all_cards if
                        card.color == PlayingCardColor.YELLOW and card.type == PlayingCardType.ONE]
            self.assertEqual(len(yellow1s), 2)
        with self.subTest():
            yellow2s = [card for card in all_cards if
                        card.color == PlayingCardColor.YELLOW and card.type == PlayingCardType.TWO]
            self.assertEqual(len(yellow2s), 2)
        with self.subTest():
            yellow3s = [card for card in all_cards if
                        card.color == PlayingCardColor.YELLOW and card.type == PlayingCardType.THREE]
            self.assertEqual(len(yellow3s), 2)
        with self.subTest():
            yellow4s = [card for card in all_cards if
                        card.color == PlayingCardColor.YELLOW and card.type == PlayingCardType.FOUR]
            self.assertEqual(len(yellow4s), 2)
        with self.subTest():
            yellow5s = [card for card in all_cards if
                        card.color == PlayingCardColor.YELLOW and card.type == PlayingCardType.FIVE]
            self.assertEqual(len(yellow5s), 2)
        with self.subTest():
            yellow6s = [card for card in all_cards if
                        card.color == PlayingCardColor.YELLOW and card.type == PlayingCardType.SIX]
            self.assertEqual(len(yellow6s), 2)
        with self.subTest():
            yellow_sides = [card for card in all_cards if
                            card.color == PlayingCardColor.YELLOW and card.type == PlayingCardType.SIDE]
            self.assertEqual(len(yellow_sides), 2)
        with self.subTest():
            yellow_d1s = [card for card in all_cards if
                          card.color == PlayingCardColor.YELLOW and card.type == PlayingCardType.DRAWONE]
            self.assertEqual(len(yellow_d1s), 1)
        with self.subTest():
            yellow_g2s = [card for card in all_cards if
                          card.color == PlayingCardColor.YELLOW and card.type == PlayingCardType.GIVETWO]
            self.assertEqual(len(yellow_g2s), 1)

        with self.subTest():
            wilds = [card for card in all_cards if
                     card.color == PlayingCardColor.WILD and card.type == PlayingCardType.WILD]
            self.assertEqual(len(wilds), 4)
        with self.subTest():
            d3s = [card for card in all_cards if
                   card.color == PlayingCardColor.WILD and card.type == PlayingCardType.DRAWTHREE]
            self.assertEqual(len(d3s), 3)
        with self.subTest():
            selfdefenses = [card for card in all_cards if
                            card.color == PlayingCardColor.WILD and card.type == PlayingCardType.SELFDEFENSE]
            self.assertEqual(len(selfdefenses), 2)
        with self.subTest():
            swaps = [card for card in all_cards if
                     card.color == PlayingCardColor.WILD and card.type == PlayingCardType.SWAP]
            self.assertEqual(len(swaps), 2)
        with self.subTest():
            jokers = [card for card in all_cards if
                      card.color == PlayingCardColor.WILD and card.type == PlayingCardType.JOKER]
            self.assertEqual(len(jokers), 1)

        with self.subTest():
            invalids = [card for card in all_cards if
                        (card.color in [PlayingCardColor.RED, PlayingCardColor.BLUE, PlayingCardColor.GREEN,
                                        PlayingCardColor.YELLOW]
                         and card.type not in [PlayingCardType.ONE, PlayingCardType.TWO, PlayingCardType.THREE,
                                               PlayingCardType.FOUR, PlayingCardType.FIVE, PlayingCardType.SIX,
                                               PlayingCardType.SIDE, PlayingCardType.DRAWONE, PlayingCardType.GIVETWO])
                        or card.color == PlayingCardColor.WILD
                        and card.type not in [PlayingCardType.WILD, PlayingCardType.DRAWTHREE,
                                              PlayingCardType.SELFDEFENSE, PlayingCardType.SWAP, PlayingCardType.JOKER]]
            self.assertEqual(len(invalids), 0)

    def test_start_of_game(self):
        game = DepadGame(3)
        self.assertEqual(0, len(game.discard_pile))
        self.assertEqual(52, len(game.pile))
        game.finished = True
        game.start_game()
        self.assertEqual(1, len(game.discard_pile))
        self.assertEqual(51, len(game.pile))

    def test_basic_turn(self):
        game = DepadGame(3)
        card_to_play = PlayingCard(PlayingCardColor.YELLOW, PlayingCardType.FOUR)
        active_player = game.players[game.active_player_index]
        active_player.give(card_to_play)
        result = None
        def assignResult():
            nonlocal result
            result = game.play_card(card_to_play)
            game.finished = True
        active_player.ask_turn = assignResult
        game.pile[-1] = PlayingCard(PlayingCardColor.YELLOW, PlayingCardType.THREE)
        game.start_game()
        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()